package com.example.parrot.pong1;

public class GameLogics {
    public int ballInitialXPositionCalculate(int screenWidth){
        int ballXPosition = screenWidth/2;
        return ballXPosition;
    }
    public int ballInitialYPositionCalculate(int screenHeight){
        int ballYPosition = screenHeight/2 -400;
        return ballYPosition;
    }
    public int movementTest(int ballYPosition,String direction){
        if(direction=="down"){
            ballYPosition = ballYPosition+25;
        }else if(direction=="up"){
            ballYPosition = ballYPosition-25;
        }
        return ballYPosition;
    }
    public int personTappedTest(String personTapDirection,int racketXPosition) {
        if (personTapDirection == "right") {
            racketXPosition = racketXPosition + 10;
        } else if (personTapDirection == "left") {
            racketXPosition = racketXPosition - 10;

        }

        return racketXPosition;
    }
}

public class GameLogicsTest {

    private GameLogics gameLogics;

    @Before
    public void setUp() throws Exception {
        gameLogics = new GameLogics();

    }

    @Test
    public void ballInitialXPositionCalculate() {
        int positionX = gameLogics.ballInitialXPositionCalculate(500);

        assertEquals(250,positionsX);
    }

    @Test
    public void ballInitialYPositionCalculate() {
        int positionY = gameLogics.ballInitialXPositionCalculate(500);

        assertEquals(-150,positionsY);
    }

    @Test
    public void movementTest() {
        int yPositionOfRacket = gameLogics.movementTest(500,"down");

        assertEquals(525,yPositionOfRacket);
    }
    @Test
    public void personTappedTest() {
        int xPositionOfRacket = gameLogics.personTappedTest("right",500);

        assertEquals(510,xPositionOfRacket);
    }

}

